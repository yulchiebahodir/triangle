def is_triangle(a, b, c):
    '''
    a + b > c
    a + c > b
    b + c > a
    non zero sides (a, b, c) 
    '''
    if a < 0 or b < 0 or c < 0:
        return Exception("Invalid")

    return (a + b) > c and (a + c) > b and (b + c) > a


print(is_triangle(9, 4, 5))
